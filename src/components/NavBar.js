import React from 'react'
import {Navbar, Nav} from 'react-bootstrap'


export default function NavBar() {
	let user = localStorage.getItem('token')
	return(
	 	<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
    		<Navbar.Brand href="/">To Do List</Navbar.Brand>
    		<Navbar.Toggle aria-controls="responsive-navbar-nav" />
    		<Navbar.Collapse id="responsive-navbar-nav">
	    		<Nav className="mr-auto">
	    		{ (!user) ? 
	    			<React.Fragment>
	    			<Nav.Link href="/login">Login</Nav.Link>
	      			<Nav.Link href="/register">Register</Nav.Link>
	      			</React.Fragment>
	      			:
	      			<Nav.Link href="/logout">Logout</Nav.Link>
	    		}
	      			
	    		</Nav>
    		</Navbar.Collapse>
  		</Navbar>
	)
}