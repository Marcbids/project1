import React,{useState, useEffect} from 'react'
import {Container, Form, Button, Table} from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function Profile() {

	const [ activelist, setActivelist] = useState([]);
	const [ done, setDone] = useState([]);
	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");

	function Update(params){
		if(params.status === true){
			const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				id : params.id,
				status: false
			})
		}
		fetch(`https://project-mabid-api.herokuapp.com/api/list/update-list`, options)
		.then(response => response.json())
		.then(list => {
			window.location.reload()
		})
		} 
		if(params.status === false) {
			const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				id : params.id,
				status: true
			})
		}
		fetch(`https://project-mabid-api.herokuapp.com/api/list/update-list`, options)
		.then(response => response.json())
		.then(list => {
			window.location.reload()
		})
		}

	}

		
	
	useEffect(() =>{
		const options = {
			headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`,
			'Content-Type' : 'application/json'  }
		}
		fetch(`https://project-mabid-api.herokuapp.com/api/list/view-active`, options)
		.then(response => response.json())
		.then(data => {
			const List = data.map((list) => {
				const params = {id: list._id, status: list.status}
				return(
					<tr key={ list._id }>
						<td><Button variant="success" type="submit" onClick={e => Update(params)}> Done </Button></td>
						<td>{ list.Title}</td>
						<td>{ list.Description }</td>
						<td>{ list.DateAdded }</td>
					</tr>
				)
			})
			setActivelist(List)
		})
	},[])

	useEffect(() =>{
		const options = {
			headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`,
			'Content-Type' : 'application/json'  }
		}
		fetch(`https://project-mabid-api.herokuapp.com/api/list/view-done`, options)
		.then(response => response.json())
		.then(data => {
			const List = data.map((list) => {
				const params = {id: list._id, status: list.status}
				return(
					<tr key={ list._id }>
						<td><Button variant="danger" type="submit" onClick={e => Update(params)}> Return </Button></td>
						<td>{ list.Title}</td>
						<td>{ list.Description }</td>
						<td>{ list.DateAdded }</td>
					</tr>
				)
			})
			setDone(List)
		})
	},[])


	function addTask(e){
		e.preventDefault();
		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				Title: `${title}`,
				Description: `${description}`,
				userId : localStorage.getItem('id')
			})
		}
		fetch(`https://project-mabid-api.herokuapp.com/api/list/`, options)
		.then(response => response.json())
		.then(list => {
			Swal.fire({
				title: 	`Task Successfull Added!`,
				icon: 'success',
				confirmButtonText: `ok`,
			}).then((result) => {
				if (result.isConfirmed) {
					window.location.reload();
				}
			})
		})
	}
			
	return(
		<React.Fragment>
			<Container>
				<h2>Things to Do</h2>
				<Form onSubmit={e=>addTask(e)}>
					<Form.Group>
		    		<Form.Label>Title</Form.Label>
		    		<Form.Control type="text" placeholder="Enter Title" value={title} onChange={e => setTitle(e.target.value)}/>
		  			</Form.Group>

		  			<Form.Group>
		    		<Form.Label>Description</Form.Label>
		    		<Form.Control type="text" placeholder="Enter Description" value={description} onChange={e => setDescription(e.target.value)}/>
		  			</Form.Group>

		  			<Button variant="primary" type="submit">
						Submit
					</Button>
				</Form>

				<label>Active</label>
				<Table striped bordered hover text-align="center">
					<thead>
						<tr>
						  	<th>
						  	
							</th>
						  	<th>Title</th>
						 	<th>Description</th>
						  	<th>Date Added</th>
						</tr>
					</thead>

					<tbody>
						{ activelist }
					</tbody>
				</Table>

				<label>Done</label>
				<Table striped bordered hover text-align="center">
					<thead>
						<tr>
						  	<th>
						  	
							</th>
						  	<th>Title</th>
						 	<th>Description</th>
						  	<th>Date Added</th>
						</tr>
					</thead>

					<tbody>
						{ done }
					</tbody>
				</Table>
			</Container>
		</React.Fragment>
	)
}