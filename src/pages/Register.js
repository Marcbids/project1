import React, {useState, useEffect} from 'react'
import {Form, Button, Container} from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function Register(){
	const [fname, setFname] = useState("");
	const [lname, setLname] = useState("");
	const [mobileno, setMobileno] = useState("");
	const [email, setEmail] = useState("");
	const [password,setPassword] = useState("");
	const [emailexist, setEmailexist] = useState(false)
	const [loading, setLoading] = useState(false)

	useEffect(()=>{
		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email
			})
		}

		fetch(`https://project-mabid-api.herokuapp.com/api/users/email-exists`, options)
		.then(response => response.json())
		.then(data =>{
			if(data === true){
				setEmailexist(true)
			} else {
				setEmailexist(false)
			}
		})
	})

	function RegAccount(e){
		setLoading(true)
		e.preventDefault();
		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email
			})
		}

		fetch(`https://project-mabid-api.herokuapp.com/api/users/email-exists`, options)
		.then(response => response.json())
		.then(data =>{
			if(data === true){
				Swal.fire({
					title: 	`Account already exist`,
					icon: 'warning',
					confirmButtonText: `ok`,
				}).then((result) => {
					if (result.isConfirmed) {
						setLoading(false)
						setEmail("");
					} 
				})
			} else {
				const options = {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						firstName: fname,
						lastName: lname,
						mobileNo: mobileno,
						email: email,
						password: password
					})
				}
				fetch(`https://project-mabid-api.herokuapp.com/api/users/`, options)
				.then(response => response.json())
				.then(data => {
					Swal.fire({
						title: 	`Account Successfull Registered!`,
						icon: 'success',
						confirmButtonText: `ok`,
					}).then((result) => {
						if (result.isConfirmed) {
							setLoading(false)
							window.location.replace('/login');
						}
					})
				})
			}
		})
	}
	return(
		<React.Fragment>
			<Container>
				<Form onSubmit={e => RegAccount(e)}>
					<Form.Group>
						<Form.Label>First Name</Form.Label>
						<Form.Control type="text" placeholder="First name" value={fname} onChange={e => setFname(e.target.value)}/>
					</Form.Group>

					<Form.Group>
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" placeholder="Last name" value={lname} onChange={e => setLname(e.target.value)}/>
					</Form.Group>

					<Form.Group>
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control type="number" placeholder="Mobile Number" value={mobileno} onChange={e => setMobileno(e.target.value)}/>
					</Form.Group>

					<Form.Group>
						
						{	(!email) ?
								<React.Fragment>
								<Form.Label>Email address</Form.Label>
								<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
								</React.Fragment>
								:
							(!emailexist) ? 
								<React.Fragment>
								<Form.Label>Email address</Form.Label>
								<p><i>Email not yet taken</i></p>
								<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} className="okayForm"/>
								</React.Fragment>
								:
								<React.Fragment>
								<Form.Label>Email address</Form.Label>
								<p><i>Email already Exist</i></p>
								<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} className="errorForm"/>
								</React.Fragment>
						}
					</Form.Group>

					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
					</Form.Group>
					<Button variant="primary" type="submit" disabled={loading}>
						Register
					</Button>
				</Form>
			</Container>
		</React.Fragment>
	)
}