import { useEffect } from 'react';
import Swal from 'sweetalert2'

export default function Logout(){
	useEffect(() => {
		//clear local storage
		localStorage.clear();
		Swal.fire({
				title: 	`Logged Out successfully`,
				icon: 'success',
				confirmButtonText: `ok`,
			}).then((result) => {
				if (result.isConfirmed) {
					window.location.replace('/');
				}
			})
	}, [])

	return null;
}