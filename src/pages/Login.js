import React, { useState} from 'react'
import {Container, Form, Button} from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';

export default function Login() {


	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	

	function authenticate(e){

		e.preventDefault();
		

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		}

		// "{ email: email, password: password }"

		fetch(`https://project-mabid-api.herokuapp.com/api/users/login`, options)
		.then(response => response.json())
		.then(data => {

			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);

			} else {
				if(data.error === 'incorrect-password') {
					Swal.fire('Authentication Failed', 'Password is incorrect', 'error')
				} else if (data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }

			}

		})

	}

	const authenticateGoogleToken = (response) => {	
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch (`https://project-mabid-api.herokuapp.com/api/users/verify-google-id-token`, options)
        .then(response => response.json())
        .then(data => {
        	console.log(data)
            if (typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } 
            
        })
    };

	const retrieveUserDetails = (accessToken) => {
		const options = {
			headers: { Authorization: `Bearer ${ accessToken }`,
			'Content-Type' : 'application/json'  }
		}

		fetch(`https://project-mabid-api.herokuapp.com/api/users/details`, options)
		.then(response => response.json())
		.then(data => {
			console.log(data)
			Swal.fire({
				title: 	`Successfully logged in!`,
				icon: 'success',
				confirmButtonText: `ok`,
			}).then((result) => {
				if (result.isConfirmed) {
					localStorage.setItem('id', data._id)
					window.location.replace('/profile');
				}
			})
			
			

		})

	}

	return(
		<React.Fragment>
			<Container>
				<Form onSubmit={e => authenticate(e)}>
					<Form.Group controlId="formBasicEmail">
						<Form.Label>Email address</Form.Label>
						<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
						<Form.Text className="text-muted">
							We'll never share your email with anyone else.
						</Form.Text>
					</Form.Group>

					<Form.Group controlId="formBasicPassword">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
					</Form.Group>
					<Button variant="primary" type="submit">
						Login
					</Button>
					<GoogleLogin 
		            	clientId="184495310670-26lu1jufh3n2dmjkjgut5b2eq6sa3lc7.apps.googleusercontent.com"
		            	buttonText="Login"
		            	onSuccess={ authenticateGoogleToken }
		            	onFailure={ authenticateGoogleToken }
		            	cookiePolicy={ 'single_host_origin' }
		            	className="w-100 text-center d-flex justify-content-center"
		            />
				</Form>
			</Container>
		</React.Fragment>
	)
}