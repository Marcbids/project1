import React from 'react'
import {Jumbotron, Container} from 'react-bootstrap'

export default function Home(){
	return(
		<Jumbotron fluid>
			<Container>
			<h1>To Do List</h1>
			<p>
			  This website is an ongoing project for you to track all the things that you must do
			</p>
			</Container>
		</Jumbotron>
	)
}